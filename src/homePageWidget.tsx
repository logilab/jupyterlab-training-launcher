import React from "react";
import styled from "styled-components";
import i18next from "./i18n";
import {
  Launcher as JupyterlabLauncher,
  ILauncher,
} from "@jupyterlab/launcher";

const CATEGORIES = ["Notebook", "Apps", "Console", "Other"];

export class Launcher extends JupyterlabLauncher {
  /**
   * Construct a new launcher widget.
   */
  constructor(options: ILauncher.IOptions) {
    super(options);
  }

  protected render(): React.ReactElement<any> | null {
    const launcherBody = super.render();
    const launcherContent = launcherBody?.props.children;
    const launcherCategories = launcherContent.props.children;

    launcherCategories[1].sort((a: any, b: any) => {
      return CATEGORIES.indexOf(a.key) - CATEGORIES.indexOf(b.key);
    });

    return (
      <div>
        <div className="homepage-header"></div>
        <div className="homepage-body">
          <Welcome>{i18next.t("Welcome to your training area")}</Welcome>
          <details>
            <summary className="homepage-summary">Informations</summary>
            <section>
              {i18next.t(
                "Here you will find thematic exercises of different levels.",
              )}
            </section>
            <section>
              {i18next.t(
                "These exercises often contain tests to validate your code and one or more solution (s).",
              )}
            </section>
            <section>
              {i18next.t(
                "We recommend that you try to solve each exercise that interests you and only look at the solution as a last resort.",
              )}
            </section>
            <p>
              {i18next.t(
                "A detailed presentation of the platform is available in the menu",
              )}
              <b>{" Help > Jupyterlab Training Tour."}</b>
            </p>
            <p>
              <b>Logilab-Training</b> {i18next.t(" is based on ")}
              <a
                target="_blank"
                rel="nofollow noreferrer noopener"
                href="https://github.com/jupyterlab/jupyterlab"
              >
                Jupyterlab
              </a>
              ,{" "}
              <a
                target="_blank"
                rel="nofollow noreferrer noopener"
                href="https://github.com/jupyterhub/jupyterhub"
              >
                Jupyterhub
              </a>{" "}
              &{" "}
              <a
                target="_blank"
                rel="nofollow noreferrer noopener"
                href="https://logilab.pages.logilab.fr/jupyterapps/"
              >
                JupyterApps
              </a>
              .
            </p>
          </details>
          <div className="jp-Launcher-content">{launcherCategories}</div>
        </div>
        <Footer>
          Powered by{" "}
          <a
            target="_blank"
            rel="nofollow noreferrer noopener"
            href="https://logilab.fr"
          >
            Logilab
          </a>
        </Footer>
      </div>
    );
  }
}

const Welcome = styled.h1`
  margin-bottom: 0.5em;
`;

const Footer = styled.div`
  float: right;
  margin: 5em;
`;
