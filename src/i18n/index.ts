import i18next from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

i18next.use(LanguageDetector).init({
  fallbackLng: "en",
  resources: {
    en: {
      translation: {
        training: "training",
        "Welcome to your training area": "Welcome to your training area",
      },
    },
    fr: {
      translation: {
        training: "formation",
        "Welcome to your training area":
          "Bienvenue sur votre espace de formation",
        "Here you will find thematic exercises of different levels.":
          "Vous trouverez ici des exercices thématiques de différents niveaux.",
        "These exercises often contain tests to validate your code and one or more solution (s).":
          "Ces exercices contiennent souvent des tests permettant de valider votre code et une ou plusieurs solution(s).",
        "We recommend that you try to solve each exercise that interests you and only look at the solution as a last resort.":
          "Nous vous conseillons d'essayer de résoudre chaque exercice qui vous intéresse et de ne regarder la solution qu'en dernier recours.",
        "A detailed presentation of the platform is available in the menu":
          "Une présentation détaillée de la plateforme est disponible dans le menu",
        " is based on ": " est basé sur ",
      },
    },
  },
});

export default i18next;
